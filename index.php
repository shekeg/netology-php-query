<?php
  require_once(__DIR__ . '/functions.php');

  $host = 'localhost';
  $db = 'global';
  $usr = 'root';
  $pass = '';
  $charset = 'utf8';

  $pdo = new PDO("mysql:host={$host};dbname={$db};charset={$charset}", $usr, $pass);

  $sql = "SELECT * FROM tasks";

  if (!empty($_POST['sort'])) {
    switch ($_POST['sort_by']) {
      case 'date_added':
        $sql = "SELECT * FROM tasks ORDER BY date_added";
        break;
      case 'is_done':
        $sql = "SELECT * FROM tasks ORDER BY is_done ASC";
        break;
      case 'description':
        $sql = "SELECT * FROM tasks ORDER BY description";
        break;
    }
  }

  if (!empty($_POST['add'])) {
    addTask($pdo, $_POST['description']);
  }
  if (!empty($_POST['save'])) {
    editTask($pdo, $_POST['id_edit'], $_POST['description_edit']);
  }
  if (!empty($_GET['action']) and $_GET['action'] === 'edit') {
    $taskForEdit = getTask($pdo, $_GET['id']);
  }
  if (!empty($_GET['action']) and $_GET['action'] === 'delete') {
    deleteTask($pdo, $_GET['id']);
  }
  if (!empty($_GET['action']) and $_GET['action'] === 'complete') {
    completeTask($pdo, $_GET['id']);
  }

  //print_r($task);
?>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" href="main.css">
</head>
<body>
  <div class="wrap">
    <h2 class="title">Список дел на сегодня</h2>
    <?php if (!empty($_GET['action']) and $_GET['action'] === 'edit'): ?>
      <form action="index.php" method="POST">
        <input type="text" name="id_edit" id="id-edit" value="<?php echo $taskForEdit['id']?>" autocomplete="off" hidden>
        <label for="description-edit">Описание</label>
        <input type="text" name="description_edit" id="description-edit" value="<?php echo $taskForEdit['description']?>" autocomplete="off">
        <input type="submit" name="save" value="Сохранить">
      </form>
    <?php endif; ?>
    <?php if (empty($_GET['action']) or $_GET['action'] !== 'edit'): ?>
      <form action="index.php" method="POST">
        <label for="description">Описание</label>
        <input type="text" name="description" id="description" autocomplete="off">
        <input type="submit" name="add" value="Добавить">
      </form>
    <?php endif; ?>
    <form action="index.php" method="POST">
      <label for="sort-by">Сортировка по: </label>
      <select name="sort_by" id="sort-by" >
        <option value="date_added">Дате добавления</option>
        <option value="is_done">Статусу</option>
        <option value="description">Описанию</option>
      </select>
      <input type="submit" name="sort" value="Отсортировать">
    </form>
    <table class="table">
      <thead>
        <tr>
          <th>Описание задачи</th>
          <th>Дата добавления</th>
          <th>Статус</th>
          <th>Действия</th>
        </tr>
      </thead>
    <?php foreach($pdo->query($sql) as $row): ?>
      <tr>
        <td><?php echo $row['description'] ?></td>
        <td><?php echo $row['date_added'] ?></td>
        <td class="<?php echo $row['is_done'] ? 'complete' : 'not-complete' ?>"><?php echo $row['is_done'] ? 'Выполнено' : 'В процессе' ?></td>
        <td>
          <a href="?action=edit&id=<?php echo $row['id'] ?>">Изменить</a>
          <a href="?action=complete&id=<?php echo $row['id'] ?>">Выполнить</a>
          <a href="?action=delete&id=<?php echo $row['id'] ?>">Удалить</a>
        </td>
      </tr>
    <?php endforeach; ?>
    </table>
  </div>
</body>
</html>