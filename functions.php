<?php
    function addTask($pdo, $description) {
      $sql = "INSERT INTO tasks (description, is_done, date_added) VALUES (:description, :is_done, :date_added)";
      $sth = $pdo->prepare($sql);
      $sth->bindParam(':description',$description);
      $sth->bindValue(':is_done', 0);
      $sth->bindValue(':date_added', date('Y-m-d h:m:s'));
      $sth->execute();
    }

    function getTask($pdo, $id) {
      $sql = "SELECT * FROM tasks WHERE id=:id";
      $sth = $pdo->prepare($sql);
      $sth->bindParam(':id', $id);
      $sth->execute();
      $task = $sth->fetch();
      return $task;
    }

    function editTask($pdo, $id, $description) {
      $sql = "UPDATE tasks SET description=:description WHERE id=:id";
      $sth = $pdo->prepare($sql);
      $sth->bindParam(':id', $id);
      $sth->bindParam(':description', $description);
      $sth->execute();
      header('Location: index.php');
    }

    function deleteTask($pdo, $id) {
      $sql="DELETE FROM tasks WHERE id=:id";
      $sth = $pdo->prepare($sql);
      $sth->bindParam(':id', $id);
      $sth->execute();
    };

    function completeTask($pdo, $id) {
      $sql="UPDATE tasks SET is_done=:is_done WHERE id=:id";
      $sth = $pdo->prepare($sql);
      $sth->bindParam(':id', $id);
      $sth->bindValue(':is_done', 1);
      $sth->execute();
    };

    
?>